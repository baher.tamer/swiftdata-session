# SwiftData Session

SwiftData is an efficient data storage solution designed for iOS, macOS, tvOS, watchOS, and visionOS applications. It facilitates the creation of custom objects, their interlinking, retrieval with filtering and sorting, and synchronization with iCloud.

## Why SwiftData
* **Latest Swift Features:** SwiftData fully utilizes the advanced features of the latest Swift features
* **SwiftUI Integration:** Seamlessly integrates with SwiftUI
* **Built on top Core Data:** Benefiting from over 20 years of development and addressing key pain points of it’s predecessor
* **Declarative data modeling:** You can define your data models in a way that is easy to read and understand
* **Automatic persistence:** SwiftData automatically persists your data to the underlying storage

## SwiftData Problems
* **Platform Limitations:** Supports only iOS 17 or later
* **Limited Documentation and Support:** SwiftData is a newer framework, so there is less documentation and support available
* **Maturity and Feature Set:** Core Data is a more mature framework, so SwiftData does not offer as many features

## Platform Compatibility
* iOS 17.0+
* iPadOS 17.0+
* macOS 14.0+
* Mac Catalyst 17.0+
* tvOS 17.0+
* watchOS 10.0+
* visionOS 1.0+

## Core Data & SwiftData Equivalents
| Core Data              | SwiftData                 |
| ---------------------- | ------------------------- |
| NSPersistentContainer  | ModelContainer            |
| NSManagedObjectContext | ModelContext              |
| NSManagedObject        | PersistentModel (\@Model) |
| NSPredicate            | #Predicate                |
| NSFetchRequest         | FetchRequest              |
| NSFetchDescriptor      | FetchDescriptor           |
| NSSortDescriptor       | SortDescriptor            |

## Resources
* **SwiftData Documentation:**
    * https://developer.apple.com/documentation/swiftdata
* **SwiftData WWDC 2023 Sessions:**
    * Meet SwiftData: https://developer.apple.com/videos/play/wwdc2023/10187/
    * Build an app with SwiftData: https://developer.apple.com/videos/play/wwdc2023/10154/
    * Migrate to SwiftData: https://developer.apple.com/videos/play/wwdc2023/10189/
    * Model your schema with SwiftData: https://developer.apple.com/videos/play/wwdc2023/10195/
    * Dive deeper into SwiftData: https://developer.apple.com/videos/play/wwdc2023/10196/
* **SwiftData by Example - Paul Hudson:**
    * https://www.hackingwithswift.com/quick-start/swiftdata
